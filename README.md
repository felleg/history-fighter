A game based around the board games "Chronology" and "Timeline". I used this project to learn more about the
`blessed` library: https://blessed.readthedocs.io/en/latest/index.html

See how long your timeline can get without cheating!

# Launching the game

`python3 history-fighter.py` or `./history-fighter.py`

# Testing the database

`python3 test_database.py` or `./test_database.py`
