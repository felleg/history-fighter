#!/bin/python3
import sys
import blessed
import pandas as pd
term = blessed.Terminal()

EVENTS_DB = "events_database.csv"

def print_event(entry):
    print("{}: {}".format(entry.year, entry.event.strip()))

def print_list(entry, cursor_position, timeline):
    # We want new element always centered, and the rest of the timeline printed around it
    # If cursor position == x. this means there are x elements above the new element, and len(timeline) - x
    # elements below.
    print(term.home + term.clear + term.move_y(term.height // 2))
    print(term.bold_yellow_reverse("{} ({})".format(entry.event.strip(), entry.category.strip())))
    print(term.move_up(cursor_position + 2))
    for i in range(cursor_position):
        # Make sure the top item is not too far away from the center to be printed
        if cursor_position - i > term.height // 2:
            continue
        print_event(timeline[i])
    print()
    for i in range(cursor_position, len(timeline)):
        print_event(timeline[i])
        if term.get_location()[0] == term.height - 2:
            break

    print(term.move_y(term.height) + 
            term.green("CONTROLS:\tup/down: Position your event on timeline.\tenter: Submit your answer"))

def gameover(entry, timeline):
    print("you lose. {} is actually dated {}.".format(entry.event.strip(), term.red(str(entry.year))))
    print("Your score is", len(timeline) - 1)
    print("bye.")
    sys.exit(0)

def victory(timeline):
    print("\nFinished timeline:")
    for t in timeline:
        print_event(t)
    print(term.green("you win.") + " That was really impressive. ")
    print("Your final score is", term.green(str(len(timeline) - 1)))
    print("bye.")
    sys.exit(0)



if __name__ == "__main__":

    events = pd.read_csv(EVENTS_DB)
    # shuffle events
    events = events.sample(frac=1)
    timeline = [events.iloc[0]]

    # This loop is to get an answer from the user (curser_position)
    for _, entry in events[1:].iterrows():  # Skip the first entry, we already have it in timeline
        with term.cbreak():
            val = None
            pressed_enter = False
            cursor_position = 0
            print_list(entry, cursor_position, timeline)
            while pressed_enter is False:
                val = term.inkey()
                if val.code == term.KEY_UP:
                    if cursor_position > 0:
                        cursor_position -= 1
                        print_list(entry, cursor_position, timeline)
                elif val.code == term.KEY_DOWN:
                    if cursor_position < len(timeline):
                        cursor_position += 1
                        print_list(entry, cursor_position, timeline)
                elif val.code == term.KEY_ENTER:
                    pressed_enter = True

        if cursor_position == 0:
            if entry.year <= timeline[cursor_position].year:
                timeline = [entry] + timeline
            else:
                gameover(entry, timeline)
        elif cursor_position == len(timeline):
            if entry.year >= timeline[cursor_position - 1].year:
                timeline += [entry]
            else:
                gameover(entry, timeline)
        else:
            if entry.year <= timeline[cursor_position].year and entry.year >= timeline[cursor_position - 1].year:
                timeline = timeline[0:cursor_position] + [entry] + timeline[cursor_position:]
            else:
                gameover(entry, timeline)

    # Make sure the timeline is complete
    if len(timeline) == len(events):
        victory(timeline)
    else:
        print("You made no errors, but your timeline is incomplete. I don't know what happened. Congrats?")
