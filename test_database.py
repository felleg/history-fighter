#!/bin/python3
'''
Run this to make sure the format of every line in the events database will be understood by the History Fighter game

If this program prints nothing, it means the database contains no errors.
'''
import pandas as pd
EVENTS_DB = "events_database.csv"

df = pd.read_csv(EVENTS_DB)
for _, row in df.iterrows():
    try:
        a = int(row.year)
        b = str(row.event).strip()
        c = str(row.category).strip()
    except Exception as e:
        print("There was an error processing row", row)
        print(e)
